# Moteino Door Monitor

This is a door monitor device that will report a door open via radio using a
Moteino with a RFM69 radio on board to a remote gateway. Check my **[ESP8266-based RFM69 to MQTT][1]** project for a ESP8266-based such gateway.

Check my **[Moteino Door Monitor][6]** post for further information about this project, including **power consumption data**.

## Hardware

The hardware is based on a **Moteino** with a **battery monitor circuitry** and a **reed switch to sense door opening**. The carrier board is a simple stripboard with 3 resistors, the reed switch, a screw terminals and a pair of wires. Strips have been cut under the Moteino. The two 1M resistors (black-brown-green) save that gap.

![Door Monitor Schematic](/schema/v2_moteino/doormonitor_moteino.png)

The battery monitoring is based on a voltage divider between a 470k and a 1M resistors. The downstream resistor is not tied to GND but to digital pin 12. When this pin is in high impedance mode the circuitry is disabled and no power is wasted. To measure voltage you first have to set it to OUTPUT and then LOW, do an analogRead in A1 and put D12 back to INPUT. Check [this post][2] about this technique.

Also please check the best position for the reed switch and the magnet. For round neodinium magnets the switch should be perpendicular to the plane of the magnet. Check next picture for clarification:

![Door Monitor with Magnet](/images/20160826_132725x.jpg)

The stripboard has a 2-pin screw terminal to connect a battery holder. A 3-AAA battery holder is hidden under the board in the picture.

## Firmware

The code is very straight forward and there are comments where I thought it was important. It uses the following libraries:

* **[RFM69_ATC][3]** by Felix Rusu and Thomas Studwell
* **[Low-Power][4]** by RocketScream

It also relies on my **RFM69Manager library** to wrap RFM69_ATC. This library manages radio setup and also message sending protocol. Messages from this node have the following format: ```<key>:<value>:<packetID>```. Packet ID is then used in the gateway to detect duplicates or missing packets from a node. If you don't want to send the packetID change the SEND_PACKET_ID value in RFM69Manager.h to 0.

## Configuration

Rename or copy the settings.h.sample file to settings.h and change its values to fit your needs. Check the descriptions for each value.

## Flashing

The project is ready to be build using **[PlatformIO][5]**.
Please refer to their web page for instructions on how to install the builder. Once installed connect the Moteino to your favourite FTDI-like programmer and:

```bash
> platformio run --target upload
```

[1]: http://tinkerman.cat/rfm69-wifi-gateway/
[2]: https://lowpowerlab.com/forum/moteino/battery-voltage-monitoring/
[3]: https://github.com/LowPowerLab/RFM69
[4]: https://github.com/rocketscream/Low-Power/
[5]: http://www.platformio.org
[6]: http://tinkerman.cat/moteino-door-monitor/
